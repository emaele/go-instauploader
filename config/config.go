package config

import (
	"errors"

	"github.com/BurntSushi/toml"
)

// Config is the bot configuration representation, read
// from a configuration file.
type Config struct {
	Username string
	Password string
}

// ReadConfig loads the values from the config file
func ReadConfig(path string) (Config, error) {
	var conf Config

	if _, err := toml.DecodeFile(path, &conf); err != nil {
		return Config{}, err
	}

	if conf.Username == "" {
		return newErr("missing username")
	} else if conf.Password == "" {
		return newErr("missing password")
	}
	return conf, nil
}

func newErr(message string) (Config, error) {
	return Config{}, errors.New(message)
}
