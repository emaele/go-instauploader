package media

import (
	"os"

	goinsta "gopkg.in/ahmdrz/goinsta.v2"
)

var (
	err error
)

// UploadPhoto uploads a photo to instagram
func UploadPhoto(user *goinsta.Instagram, path string, caption string) (id string, err error) {

	photo, err := os.Open(path)
	if err != nil {
		return
	}

	posted, err := user.UploadPhoto(photo, caption, 10, 0)
	if err != nil {
		return
	}
	return posted.Code, nil
}
