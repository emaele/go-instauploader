package utility

import (
	"log"

	"github.com/fatih/color"
)

// RedLog is a log.Println with red in it
func RedLog(s string) {
	log.Println(color.RedString(s))
}

// GreenLog is a log.Println with green in it
func GreenLog(s string) {
	log.Println(color.GreenString(s))
}

// YellowLog is a log.Println with yellow in it
func YellowLog(s string) {
	log.Println(color.YellowString(s))
}
