package main

import (
	"flag"
	"fmt"
	"log"

	conf "gitlab.com/emaele/go-instauploader/config"
	"gitlab.com/emaele/go-instauploader/media"
	"gitlab.com/emaele/go-instauploader/utility"
	goinsta "gopkg.in/ahmdrz/goinsta.v2"
)

var (
	config         conf.Config
	err            error
	configFilePath string
	photoPath      string
	caption        string
)

func main() {

	setCLIParams()

	config, err = conf.ReadConfig(configFilePath)
	if err != nil {
		utility.RedLog(err.Error())
	}

	// Login to instagram
	myUser := goinsta.New(config.Username, config.Password)
	err = myUser.Login()

	// Login message
	if err != nil {
		utility.RedLog(err.Error())
		log.Panic()
	} else {
		utility.YellowLog("Logged in")
	}

	// Upload photo
	ID, err := media.UploadPhoto(myUser, photoPath, caption)
	if err != nil {
		utility.RedLog(err.Error())
	} else {
		utility.GreenLog(fmt.Sprintf("Posted! Link: instagram.com/p/%s", ID))
	}

	// Logout
	myUser.Logout()

}

func setCLIParams() {
	flag.StringVar(&configFilePath, "config", "./config.toml", "configuration file path")
	flag.StringVar(&photoPath, "upload", "", "path of the file to upload")
	flag.StringVar(&caption, "caption", "", "caption of the photo")
	flag.Parse()
}
